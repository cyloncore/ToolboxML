#include "ToolboxMLPlugin.h"

#include <QtQml>

#include <ToolboxML/DefaultViewInterface.h>
#include <ToolboxML/ProxyTool.h>
#include <ToolboxML/ToolAction.h>
#include <ToolboxML/Tool.h>
#include <ToolboxML/ToolController.h>
#include <ToolboxML/ToolEvents.h>

using namespace ToolboxML;

void ToolboxMLPlugin::registerTypes(const char* uri)
{
  Q_ASSERT(uri == QLatin1Literal("ToolboxML"));
  qmlRegisterUncreatableType<AbstractTool>(uri, 1, 0, "AbstractTool", "It is a virtual class");
  qmlRegisterUncreatableType<MouseToolEvent>(uri, 1, 0, "MouseToolEvent", "Not useful");
  qmlRegisterUncreatableType<WheelToolEvent>(uri, 1, 0, "WheelToolEvent", "Not useful");
  
  qmlRegisterType<DefaultViewInterface> (uri, 1, 0, "DefaultViewInterface");
  qmlRegisterType<Tool>                 (uri, 1, 0, "Tool");
  qmlRegisterType<ToolAction>           (uri, 1, 0, "ToolAction");
  qmlRegisterType<ToolController>       (uri, 1, 0, "ToolController");
  qmlRegisterType<ProxyTool>            (uri, 1, 0, "ProxyTool");

}
