import QtQuick 2.0
import QtQuick.Controls 2.3

ActionGroup
{
  id: root
  exclusive: true
  property QtObject defaultTool: null
  property QtObject tool: checkedAction ? checkedAction.tool : defaultTool
  property list<ToolAction> toolsActions
  default property alias defaultActionsProperty: root.toolsActions
  function addToolAction(act)
  {
    toolsActions.push(act)
    __addToolAction(act)
  }
  function __addToolAction(act)
  {
    root.addAction(act)
    act.group = root
  }
  Component.onCompleted: {
    for(var a in root.toolsActions)
    {
      var act = root.toolsActions[a]
      root.__addToolAction(act)
    }
  }
}
