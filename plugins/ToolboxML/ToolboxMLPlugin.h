#pragma once

#include <QQmlExtensionPlugin>

class ToolboxMLPlugin: public QQmlExtensionPlugin {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "ToolboxML/1.0")
public:
  
  void registerTypes(const char* uri) override;
};
