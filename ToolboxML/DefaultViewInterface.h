#pragma once

#include "AbstractViewInterface.h"

namespace ToolboxML
{
  /**
   * Provides an implementation of \ref AbstractViewInterface for simple translation/zoom.
   */
  class DefaultViewInterface : public QObject, public AbstractViewInterface
  {
    Q_OBJECT
    Q_PROPERTY(QPointF translation READ translation WRITE setTranslation NOTIFY translationChanged)
    Q_PROPERTY(qreal zoom READ zoom WRITE setZoom NOTIFY zoomChanged)
  public:
    DefaultViewInterface();
    ~DefaultViewInterface();
  public:
    Q_INVOKABLE QPointF fromView(const QPointF & _point) const override;
    Q_INVOKABLE QPointF toView(const QPointF & _point) const override;
  public:
    QPointF translation() const;
    void setTranslation(const QPointF& _point);
    qreal zoom() const;
    void setZoom(qreal _zoom);
  private:
    void update();
  signals:
    void transformationChanged();
    void translationChanged();
    void zoomChanged();
  private:
    struct Private;
    Private* const d;
  };
}
