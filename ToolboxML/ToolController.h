#ifndef _TOOLBOXML_TOOLCONTROLLER_H_
#define _TOOLBOXML_TOOLCONTROLLER_H_

#include <QQuickItem>

namespace ToolboxML
{
  class AbstractTool;
  class AbstractViewInterface;
  /**
   * The \ref ToolController handles input events, convert them from the view coordinates to the document coordinates and dispatch them to the current tool.
   */
  class ToolController : public QQuickItem
  {
    Q_OBJECT
    /**
     * Property that holds the current tool.
     */
    Q_PROPERTY(ToolboxML::AbstractTool* tool READ tool WRITE setTool NOTIFY toolChanged)
    /**
     * Property that holds the current interface to the vie.
     */
    Q_PROPERTY(QObject* viewInterface READ viewInterfaceASQObject WRITE setViewInterface NOTIFY viewInterfaceChanged)
  public:
    ToolController(QQuickItem* parent = 0);
    virtual ~ToolController();
    AbstractTool* tool() const;
    void setTool(AbstractTool* _tool);
    AbstractViewInterface* viewInterface() const;
    void setViewInterface(AbstractViewInterface* _viewInterface);
  private:
    QObject* viewInterfaceASQObject() const;
    void setViewInterface(QObject* _object);
  signals:
    void toolChanged();
    void currentFeaturesSourceChanged();
    void viewInterfaceChanged();
  protected:
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;
    void hoverEnterEvent(QHoverEvent* event) override;
    void hoverMoveEvent(QHoverEvent* event) override;
    void hoverLeaveEvent(QHoverEvent* event) override;
  private slots:
    void toolHoverEnabledHasChanged();
  private:
    struct Private;
    Private* const d;
  };
}

#endif

