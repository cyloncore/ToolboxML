#include "DefaultViewInterface.h"

#include <QMatrix>

using namespace ToolboxML;

struct DefaultViewInterface::Private
{
  QPointF translation;
  qreal zoom;
  QMatrix fromViewT, toViewT;
};

DefaultViewInterface::DefaultViewInterface() : d(new Private)
{
}

DefaultViewInterface::~DefaultViewInterface()
{
  delete d;
}

void DefaultViewInterface::setTranslation(const QPointF& _point)
{
  if(d->translation != _point)
  {
    d->translation = _point;
    update();
    emit(translationChanged());
  }
}

QPointF DefaultViewInterface::translation() const
{
  return d->translation;
}

void DefaultViewInterface::setZoom(qreal _zoom)
{
  if(d->zoom != _zoom)
  {
    d->zoom = _zoom;
    update();
    emit(zoomChanged());
  }
}

qreal DefaultViewInterface::zoom() const
{
  return d->zoom;
}

void DefaultViewInterface::update()
{
  d->fromViewT = QMatrix();
  d->fromViewT.translate(d->translation.x(), d->translation.y());
  d->fromViewT.scale(d->zoom, d->zoom);
  d->toViewT = d->fromViewT.inverted();
}

QPointF DefaultViewInterface::fromView(const QPointF & _point) const
{
  return d->fromViewT.map(_point);
}

QPointF DefaultViewInterface::toView(const QPointF & _point) const
{
  return d->toViewT.map(_point);
}

#include "moc_DefaultViewInterface.cpp"
