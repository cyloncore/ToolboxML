#pragma once

class QPointF;

namespace ToolboxML
{
  /**
   * An \ref AbstractViewInterface provides an interface between the view and the document.
   * It is mainly used to convert coordinates between view and document.
   */
  class AbstractViewInterface
  {
  public:
    AbstractViewInterface();
    virtual ~AbstractViewInterface();
    /**
     * Convert from document coordinate to view coordinate.
     */
    virtual QPointF toView(const QPointF& _point) const = 0;
    virtual QPointF fromView(const QPointF& _point) const = 0;
  };
}

#include <QtPlugin>

Q_DECLARE_INTERFACE(ToolboxML::AbstractViewInterface, "ToolBoxML.AbstractViewInterface")
